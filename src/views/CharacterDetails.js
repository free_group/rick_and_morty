import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { instance } from '../api/rickAndMortyApi';
import './characterDetails.css';
import { ChevronLeft } from '@material-ui/icons';


import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Button } from '@material-ui/core';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  container: {
    width: 700,
    maxWidth: 700
  }
});

function CharacterDetails() {
  const classes = useStyles();
  const { id } = useParams();
  const history = useHistory();
  const [character, setCharacter] = useState({
    data: {},
    episodes: []
  });

  useEffect(() => {
    async function fetchData() {
      const characterDataResult = await instance.get(`/character/${id}`).then(result => result.data);

      
      const episodesNum = characterDataResult.episode.map(item => {
        return item.split('episode/')[1];
      });

      const episodes = await instance.get(`/episode/${episodesNum.join(',')}`).then(result => result.data);
      setCharacter({
        data: characterDataResult,
        episodes: episodesNum.length === 1 ? [episodes] : episodes
      })
    }

    fetchData();
  });

  function renderTable() {
    const { episodes } = character;
    if (episodes.length) {
      return episodes.map((ep, index) => (
        <TableRow key={index}>
          <TableCell>{ep.id}</TableCell>
          <TableCell>{ep.air_date}</TableCell>
          <TableCell>{ep.episode}</TableCell>
          <TableCell>{ep.name}</TableCell>
        </TableRow>
      ))
    }
    return null
  }

  return (
    <>
      <div id='container'>
        <div id='name'>Nombre: <b>{character.data.name}</b></div>
        <div id='gender'>Género: <b>{character.data.gender}</b></div>
        <div id='return'>
          <Button variant="contained" color="primary" onClick={() => history.push('/')}>
            <ChevronLeft /> Regresar
          </Button>
        </div>
        <div id='image'><img alt={character.data.name} src={character.data.image} /></div>
      </div>
      <TableContainer className={classes.container} component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Aire</TableCell>
              <TableCell>Episodio</TableCell>
              <TableCell>Nombre</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              renderTable()
            }
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default CharacterDetails;
