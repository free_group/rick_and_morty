import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { instance } from '../api/rickAndMortyApi';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  container: {
    width: 700,
    maxWidth: 700
  }
});

function CharactersView() {
  const classes = useStyles();
  const [characters, setCharacters] = useState([]);

  useEffect(() => {
    instance.get('/character/?page=1').then(result => {
      console.warn(result);
      setCharacters(result.data.results);
    });
  }, []);

  return (
    <div>
      <h1 style={styles.header}>Listado de personajes</h1>
      <TableContainer className={classes.container} component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Nombre</TableCell>
              <TableCell>Especie</TableCell>
              <TableCell>Género</TableCell>
              <TableCell>Estatus</TableCell>
              <TableCell>Imagen</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {characters.map((character, index) => (
              <TableRow key={index}>
                <TableCell>{character.id}</TableCell>
                <TableCell>{character.name}</TableCell>
                <TableCell>{character.species}</TableCell>
                <TableCell>{character.gender}</TableCell>
                <TableCell>{character.status}</TableCell>
                <TableCell>
                  <Link to={`/character/${character.id}`}>
                    <img
                      style={styles.imageStyle}
                      alt={`imagen de ${character.name}`}
                      src={character.image}
                    />
                  </Link>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

const styles = {
  imageStyle: {
    width: 70,
    border: '2pt solid black',
    borderRadius: '50%'
  },
  header: {
    display: 'flex',
    justifyContent: 'center',
  }
}

export default CharactersView;
