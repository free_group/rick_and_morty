import React from 'react';
import { HashRouter, Switch, Route, Redirect } from 'react-router-dom';
import CharactersView from './views/CharactersView';
import CharacterDetails from './views/CharacterDetails';

function App() {
  return (
    <HashRouter>
      <Switch>
        <Route path='/character/:id' component={CharacterDetails} />
        <Route path='/' component={CharactersView}/>
        <Redirect to='/' />
      </Switch>
    </HashRouter>
  );
};

export default App;
